#### Lista de Comandos nomad


Inicia o Nomad em modo Dev 
```bash
nomad agent -dev -bind 0.0.0.0 -log-level INFO
```

Gera token
```bash
nomad acl bootstrap
```
Deletar um token

```bash
sudo nomad acl token delete "token"
```
Rodar um job

```bash
sudo nomad run "jobname.hcl" 
```
Limpar alocações não utilizadas 

```bash
sudo nomad system gc
```


