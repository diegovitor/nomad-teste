job "traefik" {
  #region      = "fortaleza"
  datacenters = ["dc1"]
  type        = "service"


  group "traefik" {
    count = 1

    network {
      mode = "host"

      port "web" {
        to = 8000
        static = 80
      }

      port  "admin"{
         to = 8080
         static = 8080
      }

      port "web-secure" {
        to = 8443
        static = 443
      }
    }

    service {
      name = "traefik"
      provider = "consul"

      check {
        name     = "traefik-alive"
        type     = "tcp"
        port     = "web"
        interval = "10s"
        timeout  = "2s"
      }

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.rule=Host(`traefik.api.localhost`)",
        "traefik.http.routers.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.entryPoints=web",
        "traefik.http.routers.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.tls=false",
        "traefik.http.services.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.loadbalancer.server.port=8080"
      ]
    }
    
    task "traefik" {
      driver = "docker"

      config {
        image        = "traefik:2.9"
        network_mode = "host"
        ports = ["admin", "web"]
  
        args = [
          "--api=true",
          "--api.dashboard=true",
          "--api.insecure=true",
          "--entrypoints.web.address=:${NOMAD_PORT_web}",
          "--entrypoints.traefik.address=:${NOMAD_PORT_admin}",
          "--entrypoints.web-secure.address=:8443",
          "--log.level=DEBUG",
          "--providers.consulcatalog=true",
          "--providers.consulcatalog.exposedByDefault=true",
          // FIXME - Endereço do Consul
          "--providers.consulcatalog.endpoint.address=10.41.101.34:8500",
          "--providers.consulcatalog.endpoint.scheme=http",
          //"--providers.consulcatalog.defaultrule=Host(`{{.Name }}.localhost`)",

          //"--providers.file.directory=/local/",
        ]
      }

      //volumes = [
      //  "local/traefik.toml:/etc/traefik/traefik.toml",
      //]
      // template {
      //   data = file("traefik/traefik.toml")
      //   destination = "local/traefik.toml"
      // }

      resources {
        cpu    = 300
        memory = 32
      }
    }
  }
}