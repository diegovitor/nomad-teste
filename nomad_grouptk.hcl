job "traefik" {
  // datacenters = ["dc1"]
  // type        = "service"
  #region      = "fortaleza"
  datacenters = ["dc1"]
  type        = "service"

  group "traefik" {
    count = 1

    network {
      mode = "host"

      port  "web"{
         static = 80
      }
      port  "admin"{
         static = 8080
      }
    }

    service {
      name = "traefik-ingress"
      
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.rule=Host(`traefik.api.localhost`)",
        "traefik.http.routers.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.entryPoints=web",
        "traefik.http.routers.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.tls=false",
        "traefik.http.services.${NOMAD_JOB_ID}-${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_ID}.loadbalancer.server.port=8080"
      ]
    }

    task "server" {
      driver = "docker"
      config {
        image = "traefik:v2.9"
        ports = ["admin", "web"]
        args = [
          "--api.dashboard=true",
          "--api.insecure=true", ### For Test only, please do not use that in production
          "--entrypoints.web.address=:${NOMAD_PORT_web}",
          "--entrypoints.traefik.address=:${NOMAD_PORT_admin}",
          // FIXME - Endereço do Consul
          # Enables connect support, otherwise only http connections would be tried
         # "--providers.consulcatalog.connectaware=true",
          # Make the communication secure by default
          #"--providers.consulcatalog.connectbydefault=true",
          "--providers.consulcatalog=true",
          "--providers.consulcatalog.exposedbydefault=false",
          "--providers.consulcatalog.servicename=traefik-ingress",
          "--providers.consulcatalog.prefix=traefik",
          "--providers.consulcatalog.endpoint.address=10.41.101.34:8500",
          "--providers.consulcatalog.endpoint.scheme=http",
          "--api=true",
          "--log.level=DEBUG",
          #"--log.filePath=/home/traefik.log",
        ]
        network_mode = "host"
      }
    }
  }
}


# Adicionar endereço local dos serviços.
#   nomad agent -dev -region fortaleza -dc homologacao.dc.ufc.br
#   nomad agent -dev -bind 0.0.0.0 -log-level INFO
#   nomad run nomad_traefik.hcl 
# - echo "127.0.0.1       whoami1.com" >> /etc/hosts;
# - echo "127.0.0.1       whoami2.com" >> /etc/hosts;
