job "whoami" {
  datacenters = ["dc1"]
  type        = "service"

  group "demo" {
    count = 1

    network { 
      port "http" {
        to = 80
      }
    }

    service {
      name = "app-portal-api-ufc-br"
      port = "http"

      meta {
        job = "${NOMAD_JOB_ID}"
        group = "${NOMAD_GROUP_NAME}"
      }

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.${NOMAD_ALLOC_ID}.rule=Host(`whoami1.com`)",
        "traefik.http.routers.${NOMAD_ALLOC_ID}.entryPoints=web",
        "traefik.http.routers.${NOMAD_ALLOC_ID}.tls=false",
        #"traefik.http.services.${NOMAD_ALLOC_ID}.loadbalancer.server.port=80"
      ]
    }

    task "server" {
      env {
        WHOAMI_PORT_NUMBER = "${NOMAD_PORT_http}"
      }

      driver = "docker"

      config {
        image = "traefik/whoami"
        ports = ["http"]
      }
    }
  }
}


# Adicionar endereço local dos serviços.
#   nomad agent -dev -bind 0.0.0.0 -log-level INFO
#   nomad run nomad_whoami.hcl 
# - echo "127.0.0.1       whoami1.com" >> /etc/hosts;
# - echo "127.0.0.1       whoami2.com" >> /etc/hosts;